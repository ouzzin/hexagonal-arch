package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.repository.AccountRepository;

import java.util.List;

public class FindAllAccount {
    private AccountRepository accountRepository;

    public FindAllAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> findAll(){
        return accountRepository.findAll();
    }
}
