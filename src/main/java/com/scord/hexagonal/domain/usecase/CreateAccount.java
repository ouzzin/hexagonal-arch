package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.beans.IBAN;
import com.scord.hexagonal.domain.repository.AccountRepository;

public class CreateAccount {
    private AccountRepository accountRepository;

    public CreateAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account create(int amount, int accountOverdraftProtection){
        var account = Account.builder()
                .iban(IBAN.generator())
                .accountOverdraftProtection(accountOverdraftProtection)
                .build();
        if(amount > 0){
            account.credit(amount);
        }

        return accountRepository.create(account);
    }
}
