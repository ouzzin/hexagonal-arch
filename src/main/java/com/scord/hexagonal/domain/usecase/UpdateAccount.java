package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.repository.AccountRepository;

public class UpdateAccount {
    private AccountRepository accountRepository;

    public UpdateAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account update(Account account){
        return accountRepository.update(account);
    }
}
