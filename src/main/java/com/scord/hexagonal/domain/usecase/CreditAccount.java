package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.beans.IBAN;
import com.scord.hexagonal.domain.repository.AccountRepository;

public class CreditAccount {
    private AccountRepository accountRepository;

    public CreditAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account credit(int amount, IBAN iban){
        return accountRepository.find(iban)
                .credit(amount);
    }
}
