package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.beans.IBAN;
import com.scord.hexagonal.domain.repository.AccountRepository;

public class FindAccount {
    private AccountRepository accountRepository;

    public FindAccount(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account find(IBAN iban){
        return accountRepository.find(iban);
    }
}
