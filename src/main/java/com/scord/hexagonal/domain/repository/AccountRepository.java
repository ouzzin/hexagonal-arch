package com.scord.hexagonal.domain.repository;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.beans.IBAN;

import java.util.List;

public interface AccountRepository {
    Account create(Account account);
    Account find(IBAN iban);
    Account update(Account account);
    List<Account> findAll();
}
