package com.scord.hexagonal.domain.beans;

import java.awt.desktop.PreferencesEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private final IBAN iban;
    private int amount;
    private int accountOverdraftProtection;
    private final List<Transaction> transactions = new ArrayList<>();

    private Account(Builder builder) {
        iban = builder.iban;
        accountOverdraftProtection = builder.accountOverdraftProtection;
    }

    public IBAN getIban() {
        return iban;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Account credit(int amount){
        this.amount+= amount;
        var transaction = Transaction.builder()
                .amount(this.amount)
                .date(LocalDateTime.now())
                .build();
        transactions.add(transaction);
        return this;
    }

    public int getAccountOverdraftProtection() {
        return accountOverdraftProtection;
    }

    public void setAccountOverdraftProtection(int accountOverdraftProtection) {
        this.accountOverdraftProtection = accountOverdraftProtection;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public static Builder builder(){
        return new Builder();
    }


    public static final class Builder {
        public int accountOverdraftProtection;
        private IBAN iban;

        public Builder() {
        }

        public Builder iban(IBAN iban) {
            this.iban = iban;
            return this;
        }

        public Builder accountOverdraftProtection(int accountOverdraftProtection) {
            this.accountOverdraftProtection = accountOverdraftProtection;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
