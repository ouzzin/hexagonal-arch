package com.scord.hexagonal.domain.beans;

import java.util.UUID;

public class IBAN {
    private String uuid;

    private IBAN() {
        this.uuid = UUID.randomUUID().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public static IBAN generator(){
        return new IBAN();
    }
}
