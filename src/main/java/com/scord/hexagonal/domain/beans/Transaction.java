package com.scord.hexagonal.domain.beans;

import java.time.LocalDateTime;

public class Transaction {
    private LocalDateTime date;
    private int amount;

    private Transaction(Builder builder) {
        date = builder.date;
        amount = builder.amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getAmount() {
        return amount;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder {
        private LocalDateTime date;
        private int amount;

        public Builder() {
        }

        public Builder date(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }
}
