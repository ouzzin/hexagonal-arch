package com.scord.hexagonal.config;

import com.scord.hexagonal.adapter.repository.InMemoryAccountRepository;
import com.scord.hexagonal.domain.repository.AccountRepository;
import com.scord.hexagonal.domain.usecase.*;

public class ManualConfig {
    private final AccountRepository accountRepository = new InMemoryAccountRepository();

    public CreateAccount createAccount() {
        return new CreateAccount(accountRepository);
    }

    public FindAccount findAccount() {
        return new FindAccount(accountRepository);
    }

    public FindAllAccount findAllAccount() {
        return new FindAllAccount(accountRepository);
    }

    public UpdateAccount updateAccount() {
        return new UpdateAccount(accountRepository);
    }

    public CreditAccount creditAccount() {
        return new CreditAccount(accountRepository);

    }
}
