package com.scord.hexagonal.adapter.repository;

import com.scord.hexagonal.domain.beans.Account;
import com.scord.hexagonal.domain.beans.IBAN;
import com.scord.hexagonal.domain.repository.AccountRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryAccountRepository implements AccountRepository {
    private Map<IBAN, Account> inMemoryDb = new HashMap<>();

    @Override
    public Account create(Account account) {
        inMemoryDb.put(account.getIban(), account);
        return account;
    }

    @Override
    public Account find(IBAN iban) {
        return inMemoryDb.get(iban);
    }

    @Override
    public Account update(Account account) {
        return inMemoryDb.put(account.getIban(), account);
    }

    @Override
    public List<Account> findAll() {
        return inMemoryDb.keySet().stream()
                .map(key -> inMemoryDb.get(key))
                .collect(Collectors.toList());
    }
}
