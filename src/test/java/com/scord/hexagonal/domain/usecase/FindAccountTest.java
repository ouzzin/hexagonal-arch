package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.config.ManualConfig;
import com.scord.hexagonal.domain.beans.IBAN;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FindAccountTest {

    @Test
    @DisplayName("when find account then return account")
    void when_find_account_then_return_account() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        var account = createAccount.create(100, 0);
        // when
        var findAccount = manualConfig.findAccount();
        var accountFound = findAccount.find(account.getIban());

        // then
        assertThat(accountFound).isNotNull();
        assertThat(accountFound).extracting("amount").isEqualTo(100);
        assertThat(accountFound.getIban()).extracting("uuid").isEqualTo(account.getIban().getUuid());
    }

    @Test
    @DisplayName("when find account who does not exist then return null")
    void when_find_account_who_does_not_exist_then_return_null() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        createAccount.create(100, 0);
        // when
        var findAccount = manualConfig.findAccount();
        var accountFound = findAccount.find(IBAN.generator());

        // then
        assertThat(accountFound).isNull();
    }
}
