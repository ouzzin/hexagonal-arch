package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.config.ManualConfig;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UpdateAccountTest {
    @Test
    void when_update_account_then_return_account() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        var account = createAccount.create(100, 0);
        var findAccount = manualConfig.findAccount();
        var accountFound = findAccount.find(account.getIban());
        // when
        var updateAccount = manualConfig.updateAccount();
        accountFound.setAmount(1000);
        var accoundUpdated = updateAccount.update(accountFound);

        // then
        assertThat(accoundUpdated).isNotNull();
        assertThat(accoundUpdated).extracting("amount").isEqualTo(1000);
        assertThat(accoundUpdated.getIban()).extracting("uuid").isEqualTo(accountFound.getIban().getUuid());
    }
}
