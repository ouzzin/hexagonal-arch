package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.config.ManualConfig;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FindAllAccountTest {
    @Test
    void when_find_all_account_then_return_all_account() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        createAccount.create(100, 0);
        createAccount.create(101, 0);
        // when
        var findAllAccount = manualConfig.findAllAccount();
        var allAccount = findAllAccount.findAll();
        // then
        assertThat(allAccount).isNotNull();
        assertThat(allAccount).hasSize(2);
    }

}
