package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.config.ManualConfig;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CreateAccountTest {

    @Test
    @DisplayName("when create account then return account")
    void when_create_account_then_return_account() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();

        // when
        var account = createAccount.create(100, 0);

        // then
        assertThat(account).isNotNull();
        assertThat(account).extracting("amount").isEqualTo(100);
        assertThat(account.getIban()).isNotNull();
    }
}
