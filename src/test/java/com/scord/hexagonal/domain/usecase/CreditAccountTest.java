package com.scord.hexagonal.domain.usecase;

import com.scord.hexagonal.config.ManualConfig;
import com.scord.hexagonal.domain.beans.Account;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CreditAccountTest {
    @Test
    @DisplayName("when credit account then return update account with transaction")
    void when_credit_account_then_return_update_account_with_transaction() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        var account = createAccount.create(100, 0);
        // when
        var creditAccount = manualConfig.creditAccount();
        var accountCredited = creditAccount.credit(1000, account.getIban());

        // then
        assertThat(accountCredited).isNotNull();
        assertThat(accountCredited).extracting("amount").isEqualTo(1100);
        assertThat(accountCredited.getTransactions()).hasSize(2);
    }

    @Test
    @DisplayName("when credit account without amount then return update account with transaction")
    void when_credit_account_without_amount_then_return_update_account_with_transaction() {
        // given
        ManualConfig manualConfig = new ManualConfig();
        var createAccount = manualConfig.createAccount();
        var account = createAccount.create(0, 0);
        // when
        var creditAccount = manualConfig.creditAccount();
        var accountCredited = creditAccount.credit(1000, account.getIban());

        // then
        assertThat(accountCredited).isNotNull();
        assertThat(accountCredited).extracting("amount").isEqualTo(1000);
        assertThat(accountCredited.getTransactions()).hasSize(1);
    }
}
