<h1> Your Task </h1>
<p>Your bank is tired of its mainframe COBOL accounting software and they hired both of you for a greenfield project in - what a happy coincidence</p>

your favorite programming language!
Your task is to show them that your TDD-fu and your new-age programming language can cope with good ole’ COBOL!

<h1>Requirements</h1>

<p>Write a class Account that offers the following methods void deposit(int) void withdraw(int) String printStatement()</p>

<h1>An example statement would be:</h1>

<p>
Date        Amount  Balance<br>
24.12.2015   +500      500<br>
23.8.2016    -100      400
</p>
